package flixel.addons.ui;
import flixel.util.FlxPoint;

class TouchPad extends TouchZone
{
	private var lastPosition:FlxPoint = FlxPoint.get();
	
	public var threshold:Int = 10;
	
	public var goLeft(get, never):Bool;
	public var goRight(get, never):Bool;
	public var goUp(get, never):Bool;
	public var goDown(get, never):Bool;
	
	
	
	private var _goLeft:Bool;
	private var _goRight:Bool;
	private var _goUp:Bool;
	private var _goDown:Bool;
	
	public function new(?Width:Float=42, ?Height:Float=42) 
	{
		super(Width, Height);
		
		onUp = onTouchUp;
		onDown = onTouch;
		onPressed = onSlide;
		
		resetMoves();
	}
	
	override public function update():Void
	{
		//resetMoves();
		
		super.update();
	}
	
	private function onTouchUp():Void
	{
		lastPosition.set();
		
		resetMoves();
	}
	private function onTouch():Void
	{
		lastPosition.copyFrom(position);
	}
	
	private function onSlide():Void
	{
		if (Math.abs(position.x - lastPosition.x) <  threshold)	return;
		
		_goLeft = (position.x < lastPosition.x);
		_goRight = (position.x > lastPosition.x);
		
		_goUp = (position.y < lastPosition.y);
		_goDown = (position.y > lastPosition.y);
		lastPosition.copyFrom(position);
	}
	
	private function resetMoves():Void
	{
		_goLeft = false;
		_goRight = false;
		_goUp = false;
		_goDown = false;
	}
	
	private function get_goLeft( ):Bool
	{
		return _goLeft;
	}
	private function get_goRight( ):Bool
	{
		return _goRight;
	}
	private function get_goUp( ):Bool
	{
		return _goUp;
	}
	private function get_goDown( ):Bool
	{
		return _goDown;
	}
}