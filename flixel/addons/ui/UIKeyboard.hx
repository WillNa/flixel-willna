package flixel.addons.ui;

import flash.events.KeyboardEvent;
import flash.ui.Keyboard;

import flixel.FlxG;
import flixel.addons.ui.FlxUIPopup;
import flixel.addons.ui.FlxUIButton;
import flixel.addons.ui.FlxUITypedButton;


class UIKeyboard extends FlxUIPopup
{
	private var ui_shift:FlxUIButton;
	private var keyLayout:KeyLayout;
	private var keyMode:KeyMode;
	
	public function new(?keyLayout:KeyLayout, ?keyMode:KeyMode) 
	{
		super();
		if (keyLayout == null)	keyLayout = QWERTY;
		if (keyMode == null)	keyMode = LETTERS;
		
		this.keyLayout = keyLayout;
		this.keyMode = keyMode;
	}
	
	public override function create():Void
	{
		var layout_id:String = "qwerty";
		switch (keyLayout)
		{
			case QWERTY:
				layout_id = "qwerty";
			case AZERTY:
				layout_id = "azerty";
			case NUMPAD:
				layout_id = "numpad";
		}
		
		if (keyLayout != NUMPAD)
		{
			switch(keyMode)
			{
				case LETTERS:
					layout_id += "_letters";
				case NUMBERS:
					layout_id += "_numbers";
				case MORE:
					layout_id += "_more";
			}
		}
		
		_xml_id = "ui/keyboard/"+layout_id;
		
		super.create();
		
		//TODO : icons
		var ui_del:FlxUIButton = cast _ui.getAsset("del");
		if (ui_del != null)
		{
			//ui_del.addIcon(icon, X, Y, center)
		}
		
		ui_shift = cast _ui.getAsset("shift");
		if (ui_shift != null)
		{
			//ui_shift.addIcon(icon, X, Y, center)
		}
	}

	public override function getEvent(id:String, target:Dynamic, data:Array<Dynamic>, ?params:Array<Dynamic>):Void 
	{
		if ( (params != null) && (id == FlxUITypedButton.CLICK_EVENT)) {
			var keyChar:String = params[0];
			var isUpper:Bool = ( (ui_shift != null) && (ui_shift.toggled) );
			keyChar = keyChar.toLowerCase();
			
			if (keyChar.length > 1)
			{
				switch(keyChar)
				{
					case "del":
						FlxG.stage.dispatchEvent( new KeyboardEvent(KeyboardEvent.KEY_DOWN, true, false, 0, Keyboard.BACKSPACE));
					case "shift":
						//done by toggle button
					case "ok":
						FlxG.stage.dispatchEvent( new KeyboardEvent(KeyboardEvent.KEY_DOWN, true, false, 0, Keyboard.ENTER));				
					case "number":
						this.keyMode = NUMBERS;
						resetUI();
					case "alpha":
						this.keyMode = LETTERS;
						resetUI();
					case "more":
						this.keyMode = MORE;
						resetUI();
					case "dquote":
						fakeKeyDown('"', false);
				}
			}
			else
			{
				if  (isUpper)	keyChar = keyChar.toUpperCase();
				fakeKeyDown(keyChar, isUpper);
			}
		}
	}
	
	private function fakeKeyDown(keyChar:String, isUpper:Bool ):Void
	{
		var keyCode:Int = KeyCodeUtil.keyCodeOf(keyChar);
		if (keyCode == -1)
		{
			//trace("unknown char "+keyChar+" => "+keyChar.charCodeAt(0) );
			keyChar = " ";
			keyCode = 32; // KeyCodeUtil.keyCodeOf(keyChar);
		}
				
		FlxG.stage.dispatchEvent( new KeyboardEvent(KeyboardEvent.KEY_DOWN, true, false, keyChar.charCodeAt(0), keyCode) );
	}
	
	private function resetUI():Void
	{
		if (_ui != null) {
			remove(_ui, true);
			_ui.destroy();
			_ui = null;
		}
		
		create();
	}
}


///http://stackoverflow.com/questions/1659444/get-keycode-from-string
// thanks Andy Li!
class KeyCodeUtil {
    	/*
    		The below codes is used for generating KEYCODES. Some formating is needed to be done by hand after getting the outputStr.

    		var outputStr:String = "";
    		function onPress(evt:KeyboardEvent):void {
    			outputStr += "'" + evt.charCode + "':" + evt.keyCode + ", //"+String.fromCharCode(evt.charCode)+"\n";
    		}
    	*/
    	static var KEYCODES = {
    			"char8": 8,		//backspace
    			"char9": 9,		//tab
    			"char13": 13,		//enter
    			"char27": 27,		//esc  
				"char32": 32,		//space
				"char33": 49,		//!
    			"char34": 222,	//"
    			"char35": 51,		//#
    			"char36": 52,		//$
    			"char37": 53,		//%
    			"char38": 55,		//&
    			"char39": 222,	//'
    			"char40": 57,		//(
    			"char41": 48,		//)
    			"char42": 56,		//*
    			"char43": 187,	//+
    			"char44": 188,	//,
    			"char45": 189,	//-
    			"char46": 190,	//.
    			"char47": 191,	///
    			"char48": 48,		//0
    			"char49": 49,		//1
    			"char50": 50,		//2
    			"char51": 51,		//3
    			"char52": 52,		//4
    			"char53": 53,		//5
    			"char54": 54,		//6
    			"char55": 55,		//7
    			"char56": 56,		//8
    			"char57": 57,		//9
    			"char58": 186,	//":
    			"char59": 186,	//;
    			"char60": 188,	//<
    			"char61": 187,	//=
    			"char62": 190,	//>
    			"char63": 191,		//?
    			"char64": 50,		//@
    			"char65": 65,		//A
    			"char66": 66,		//B
    			"char67": 67,		//C
    			"char68": 68,		//D
    			"char69": 69,		//E
    			"char70": 70,		//F
    			"char71": 71,		//G
    			"char72": 72,		//H
    			"char73": 73,		//I
    			"char74": 74,		//J
    			"char75": 75,		//K
    			"char76": 76,		//L
    			"char77": 77,		//M
    			"char78": 78,		//N
    			"char79": 79,		//O
    			"char80": 80,		//P
    			"char81": 81,		//Q
    			"char82": 82,		//R
    			"char83": 83,		//S
    			"char84": 84,		//T
    			"char85": 85,		//U
    			"char86": 86,		//V
    			"char87": 87,		//W
    			"char88": 88,		//X
    			"char89": 89,		//Y
    			"char90": 90,		//Z
    			"char91": 219,	//[
    			"char92": 220,	// \
    			"char93": 221,	//]
    			"char94": 54,		//^
    			"char95": 189,	//_
    			"char96": 192,	//`
    			"char97": 65,	//a
    			"char98": 66,		//b
    			"char99": 67,		//c
    			"char100": 68,	//d
    			"char101": 69,	//e
    			"char102": 70,	//f
    			"char103": 71,	//g
    			"char104": 72,	//h
    			"char105": 73,	//i
    			"char106": 74,	//j
    			"char107": 75,	//k
    			"char108": 76,	//l
    			"char109": 77,	//m
    			"char110": 78,	//n
    			"char111": 79,	//o
    			"char112": 80,	//p
    			"char113": 81,	//q
    			"char114": 82,	//r
    			"char115": 83,	//s
    			"char116": 84,	//t
    			"char117": 85,	//u
    			"char118": 86,	//v
    			"char119": 87,	//w
    			"char120": 88,	//x
    			"char121": 89,	//y
    			"char122": 90,	//z
    			"char123": 219,	//{
    			"char124": 220,	//|
    			"char125": 221,	//}
    			"char126": 192,	//~
    			"char127": 46,	//delete
				"char163": 186, //£
				"char164": 186, //¤
				"char165": 92, //¥
				"char8364": 69, //€
				

    		}

    	/**
    	 * @param char Can be String or charCode(Number).
    	 * @param numpad If set to true, the function will return the keyCode for numpad. Ignored if numpad have no such key.
    	 */
    	static public function keyCodeOf(char:String, numpad:Bool = false):Int {
    		var charCode:Int = char.charCodeAt(0);
    		

    		if (numpad){
    			if (charCode >= 48 && charCode <= 57){ //0-9
    				return charCode + 48;
    			} else if (charCode == 43){
    				return Keyboard.NUMPAD_ADD;
    			} else if (charCode == 46){
    				return Keyboard.NUMPAD_DECIMAL;
    			} else if (charCode == 47){
    				return Keyboard.NUMPAD_DIVIDE;
    			} else if (charCode == 13){
    				return Keyboard.NUMPAD_ENTER;
    			} else if (charCode == 42){
    				return Keyboard.NUMPAD_MULTIPLY;
    			} else if (charCode == 45){
    				return Keyboard.NUMPAD_SUBTRACT;
    			}
    		}
			
			var keycode = Reflect.field(KEYCODES, "char" + Std.string(charCode) );
			if (keycode == null)
				return -1;
			else
				return cast keycode;
    	}

    }
enum KeyLayout {
	QWERTY;
	AZERTY;
	NUMPAD;
}

enum KeyMode {
	LETTERS;
	NUMBERS;
	MORE;
}