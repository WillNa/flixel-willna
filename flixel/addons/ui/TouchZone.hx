package flixel.addons.ui;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.input.touch.FlxTouch;
import flixel.util.FlxPoint;
import flixel.util.FlxRect;
import flixel.util.FlxDestroyUtil;
import flixel.ui.FlxButton;

import haxe.Log;

/**
 * ...
 * @author 
 */
class TouchZone extends FlxSprite
{	
	
	public var onUp:Void->Void;
	public var onDown:Void->Void;
	public var onOver:Void->Void;
	public var onPressed:Void->Void;
	
	
	public var status(get, never):Int;
	public var position(get, never):FlxPoint;
	public var positionToCenter(get, never):FlxPoint;
	public var percentage(get, never):FlxPoint;
	
	
	private var _status:Int = FlxButton.NORMAL;
	private var _pos:FlxPoint = FlxPoint.get();
	private var _per:FlxPoint = FlxPoint.get();
	private var _fromCenter:FlxPoint = FlxPoint.get();
	
	
	private var _zone:FlxRect = FlxRect.get();
	private var _center:FlxPoint = FlxPoint.get();
	
	#if !FLX_NO_TOUCH
	private var _currentTouch:FlxTouch = null;
	#end
	
	public function new(?Width:Float = 42, ?Height:Float = 42) 
	{
		super();
		
		this.makeGraphic( Std.int(Width), Std.int(Height) );
		
		scrollFactor.set();
		solid = false;
		moves = false;
		visible = false;
		
		#if !FLX_NO_DEBUG
		//ignoreDrawDebug = true;
		#end
	}
	
	override public function destroy():Void
	{
		super.destroy();
		
		_zone = FlxDestroyUtil.put(_zone);
		
		onUp = null;
		onDown = null;
		onOver = null;
		onPressed = null;
		
		#if !FLX_NO_TOUCH
		_currentTouch = null;
		#end
	}
	
	override public function update():Void 
	{
		var _point:FlxPoint = FlxPoint.get();
		var validPoint:Bool = false;
		
		#if !FLX_NO_TOUCH
		var touch:FlxTouch = null;
		if (_currentTouch == null)
		{
			for (touch in FlxG.touches.list)
			{		
				var touchInserted:Bool = false;
				_point = touch.getWorldPosition(FlxG.camera, _point);
				if ( checkPosition(_point, touch.pressed, touch.justPressed, touch.justReleased) )
				{
					_currentTouch = touch;
					validPoint = true;
					break; //we don't care about the other touch, only first one is used
				}
			}
		}
		else
		{
			//Log.trace(_currentTouch.getScreenPosition() + " Pressed :"+_currentTouch.pressed +" JustPressed :"+_currentTouch.justPressed +" JustReleased :"+ _currentTouch.justReleased);
			_point = _currentTouch.getWorldPosition(FlxG.camera, _point);
			if ( checkPosition(_point, _currentTouch.pressed, _currentTouch.justPressed, _currentTouch.justReleased) )
			{
				validPoint = true;
			}
		}
		
		#end
		
		#if !FLX_NO_MOUSE
		_point.set(FlxG.mouse.screenX, FlxG.mouse.screenY);
			
		if ( checkPosition(_point, FlxG.mouse.pressed, FlxG.mouse.justPressed, FlxG.mouse.justReleased) )
		{
			validPoint = true;
		}
		#end
		
		
		if ( validPoint == false )
		{
			if (_status != FlxButton.NORMAL ) //(HIGHLIGHT or PRESSED)
			{
				#if !FLX_NO_MOUSE
				if ( onOver != null)
					onOver();
				#end
				
				if ( (_status == FlxButton.PRESSED) && (onUp != null) )
					onUp();
		
				_status = FlxButton.NORMAL;
			}
			
			#if !FLX_NO_TOUCH
			_currentTouch = null;
			#end
			
			_pos.set();
			_per.set();
			_fromCenter.set();
		}
		
		super.update();
	}
	
	private function checkPosition(TouchPoint:FlxPoint, Pressed:Bool, JustPressed:Bool, JustReleased:Bool, ?Touch:FlxTouch):Bool
	{
		if ( !_zone.containsFlxPoint(TouchPoint))	return false;
		
		
		TouchPoint.copyTo(_pos);
		_per.set(_pos.x / width, _pos.y / height);
		
		var dx:Float = TouchPoint.x - _center.x;
		var dy:Float = TouchPoint.y - _center.y;
		
		_fromCenter.set(dx, dy);
				
		
		if (Pressed)
		{
			if (JustPressed)
			{
				_status = FlxButton.PRESSED;
				
				if (onDown != null)
				{
					onDown();
				}
			}	
			
			if (onPressed != null)
			{
				onPressed();						
			}
		}	
		else
		{
			_status = FlxButton.HIGHLIGHT;
			
			#if !FLX_NO_TOUCH
			if (onUp != null)
			{
				onUp();
			}		
			return false;
			#end
			
			
			if (JustReleased)
			{				
				if (onUp != null)
				{
					onUp();
				}		
			}	
			
			if (onOver != null)
			{
				onOver();
			}
		}
		
		return true;
	}
	
	

	
	private function createZone():Void
	{
		_zone.set(x, y, width, height);
		_center.set(x + width / 2, y + height / 2);
	}
	
	
	override private function set_x(X:Float):Float
	{
		super.set_x(X);
		createZone();
		
		return X;
	}
	
	override private function set_y(Y:Float):Float
	{
		super.set_y(Y);
		createZone();
		
		return Y;
	}
	
	private function get_status( ):Int
	{
		return _status;
	}
	private function get_position( ):FlxPoint
	{
		return _pos;
	}
	private function get_positionToCenter( ):FlxPoint
	{
		return _fromCenter;
	}
	private function get_percentage( ):FlxPoint
	{
		return _per;
	}
	
}