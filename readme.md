Flixel WillNa
=============

This haxe lib include some UI components we, at WillNa, use for our games.

At this time, we'd like to share with you

1/ UIKeyboard
A replacement for the native softkeyboard, which is so difficult to rise on some platform.
You now control your softkeyboard, just define your layout(s) and UIKeyboard will send the right KeyboardEvent for you

Delete icon by Cole Bemis (http://colebemis.com/)
https://www.iconfinder.com/icons/227590/delete_icon
	

2/ TouchPad / TouchZone
Class to make it easy to add touch control on a game
	
	
Thanks to Lars for his Flixel UI and for his time on Twitter


License
----

MIT


**Free Software, Hell Yeah!**