package;

import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.ui.Keyboard;
import flash.text.TextField;
import flash.text.TextFieldType;
import flixel.addons.ui.UIKeyboard;
import flixel.FlxG;
import flixel.FlxSubState;
import flixel.FlxSprite;
import flixel.addons.ui.FlxInputText;
import flixel.util.FlxColor;


class InputSubState extends FlxSubState
{	
	
	private var ti:FlxInputText;
	
	override public function create():Void
	{
		super.create();
		
		ti = new FlxInputText(100, 50, 150, "", 10, 0xFF0000FF, FlxColor.TRANSPARENT, true );
		ti.font = "assets/arial.ttf";
		//ti.font = "assets/nokiafc22.ttf";
		ti.filterMode = FlxInputText.ONLY_ALPHANUMERIC;
		ti.fieldBorderColor = FlxColor.TRANSPARENT;
		ti.caretWidth = 8;
		ti.maxLength = 12;
		ti.lines = 1;
		ti.hasFocus = true;		
		add(ti);

		openSubState( new UIKeyboard(QWERTY, LETTERS) ) ;		
		FlxG.stage.addEventListener(KeyboardEvent.KEY_DOWN, onInput);
	}
	
	override public function destroy():Void
	{
		FlxG.stage.removeEventListener(KeyboardEvent.KEY_DOWN, onInput);

		super.destroy();
	}
	
	private function onStageFocus(event:FocusEvent):Void
	{
		trace("Focus "+event.type+" "+FlxG.stage.focus);
		ti.hasFocus = true;
	}

	private function onInput(event:KeyboardEvent):Void
	{		
		if (event.keyCode == Keyboard.ENTER)
		{
			var name:String = ti.text;
			if (name.length==0)	name = "YOU";
			
			this.close();
		}
	}
}