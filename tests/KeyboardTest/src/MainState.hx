package ;

import flixel.FlxG;
import flixel.FlxState;

import flash.events.KeyboardEvent;
import flash.ui.Keyboard;
//import flixel.willna.ui.FlxVirtualKeyboard;


class MainState extends FlxState
{

	override public function create():Void
	{
		super.create();		
		FlxG.mouse.useSystemCursor = true;
		//add( new FlxVirtualKeyboard() );
		
		this.bgColor = 0xff00ff00;
		
		this.openSubState(new InputSubState());	
	}
	
}